#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "concurrent_list.h"

struct node
{
  int value;
  struct node *next;
  pthread_mutex_t lock;
};

struct list
{
  struct node *head;
  pthread_mutex_t lock;
};

void print_node(node *node)
{
  // DO NOT DELETE
  if (node)
  {
    printf("%d ", node->value);
  }
}

list *create_list()
{
  list *newList = (list *)malloc(sizeof(list));
  if (newList)
  {
    newList->head = NULL;
    pthread_mutex_init(&(newList->lock), NULL);
  }
  return newList;
}

void delete_list(list *list)
{
  if (list)
  {
    pthread_mutex_lock(&(list->lock));
    struct node *current = list->head;
    while (current)
    {
      struct node *temp = current;
      current = current->next;
      pthread_mutex_destroy(&(temp->lock));
      free(temp);
    }
    list->head = NULL; // Make sure the head is set to NULL
    pthread_mutex_unlock(&(list->lock));
    pthread_mutex_destroy(&(list->lock));
    free(list);
  }
}

void insert_value(list *list, int value)
{
  if (!list)
    return;

  struct node *newNode = (struct node *)malloc(sizeof(struct node));
  if (!newNode)
    return;
  newNode->value = value;
  pthread_mutex_init(&(newNode->lock), NULL);

  pthread_mutex_lock(&(list->lock));

  struct node *current = list->head;
  struct node *prev = NULL;

  while (current && current->value < value)
  {
    if (prev)
      pthread_mutex_unlock(&(prev->lock));
    prev = current;
    pthread_mutex_lock(&(current->lock));
    current = current->next;
  }

  newNode->next = current;
  if (prev)
  {
    prev->next = newNode;
    pthread_mutex_unlock(&(prev->lock));
  }
  else
  {
    list->head = newNode;
  }

  pthread_mutex_unlock(&(list->lock));
  if (current)
    pthread_mutex_unlock(&(current->lock));
}

void remove_value(list *list, int value)
{
  if (!list)
    return;

  pthread_mutex_lock(&(list->lock));

  struct node *current = list->head;
  struct node *prev = NULL;

  while (current && current->value != value)
  {
    if (prev)
      pthread_mutex_unlock(&(prev->lock));
    prev = current;
    pthread_mutex_lock(&(current->lock));
    current = current->next;
  }

  if (current && current->value == value)
  {
    if (prev)
    {
      prev->next = current->next;
      pthread_mutex_unlock(&(prev->lock));
    }
    else
    {
      list->head = current->next;
    }
    pthread_mutex_destroy(&(current->lock));
    free(current);
  }

  pthread_mutex_unlock(&(list->lock));
  if (current)
    pthread_mutex_unlock(&(current->lock));
}

void print_list(list *list)
{
  if (!list)
    return;

  pthread_mutex_lock(&(list->lock));
  struct node *current = list->head;

  while (current)
  {
    pthread_mutex_lock(&(current->lock));
    print_node(current);
    struct node *temp = current;
    current = current->next;
    pthread_mutex_unlock(&(temp->lock));
  }

  pthread_mutex_unlock(&(list->lock));
  printf("\n"); // DO NOT DELETE
}

void count_list(list *list, int (*predicate)(int))
{
  int count = 0; // DO NOT DELETE

  if (!list)
  {
    printf("%d items were counted\n", count); // DO NOT DELETE
    return;
  }

  pthread_mutex_lock(&(list->lock));
  struct node *current = list->head;

  while (current)
  {
    pthread_mutex_lock(&(current->lock));
    if (predicate(current->value))
    {
      count++;
    }
    struct node *temp = current;
    current = current->next;
    pthread_mutex_unlock(&(temp->lock));
  }

  pthread_mutex_unlock(&(list->lock));
  printf("%d items were counted\n", count); // DO NOT DELETE
}
