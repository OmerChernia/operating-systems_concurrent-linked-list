
This project implements a concurrent linked list in C, which allows multiple threads to safely perform operations such as insertions, deletions, and traversals concurrently. The linked list is synchronized using pthreads to ensure thread safety.

## Features

- Concurrent linked list supporting multiple threads
- Functions to create, delete, print, insert, and remove elements
- Counting elements based on a predicate
- Proper synchronization using pthread mutexes

## Requirements

- GCC (GNU Compiler Collection)
- pthreads library

## Compilation

To compile the project, use the following command:

```sh
gcc -g -Werror -pthread -std=c99 -o test test.c concurrent_list.c
```

## Usage

To run the program, execute the compiled binary:

```sh
./test
```

You can enter commands interactively to manipulate the linked list. The following commands are supported:

- `create_list`: Create a new linked list.
- `delete_list`: Delete the existing linked list.
- `print_list`: Print the elements of the linked list.
- `insert_value <value>`: Insert a new element with the specified value into the linked list.
- `remove_value <value>`: Remove the first element with the specified value from the linked list.
- `count_greater <value>`: Count the number of elements greater than the specified value.
- `join`: Wait for all previously created threads to finish.
- `exit`: Exit the program.

### Example Usage

```sh
create_list
insert_value 5
insert_value -3
insert_value 10
print_list
insert_value 7
insert_value 15
print_list
count_greater 6
remove_value 5
print_list
delete_list
print_list
exit
```

## Files

- `concurrent_list.c`: Implementation of the concurrent linked list.
- `concurrent_list.h`: Header file defining the linked list structures and function prototypes.
- `test.c`: Test file with the main function and command handling logic.

## Project Structure

```
.
├── concurrent_list.c
├── concurrent_list.h
├── test.c
└── README.md
```